const withCSS = require('@zeit/next-css')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { ANALYZE } = process.env
const path = require('path')

module.exports = withCSS({
  webpack: function (config, { isServer }) {
    if (ANALYZE) {
      config.plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerPort: isServer ? 8888 : 8889,
        openAnalyzer: true
      }))
    }

    config.resolve.alias.comp = path.resolve(__dirname, 'comp')
    config.resolve.alias.glUtil = path.resolve(__dirname, 'gl-util')

    config.module.rules.push({
        test: /\.vert$/,
        use: 'raw-loader'
      })
    config.module.rules.push({
        test: /\.frag$/,
        use: 'raw-loader'
      })

    return config
  }
})
