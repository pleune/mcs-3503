attribute vec4 vPosition;
uniform float rot;
varying lowp vec3 color;

void main(){
    float d = sqrt(vPosition.x*vPosition.x + vPosition.y*vPosition.y);
    vec4 newPosition = vPosition;
    newPosition.x = vPosition.x * cos(d+rot) - vPosition.y * sin(d+rot);
    newPosition.y = vPosition.x * sin(d+rot) + vPosition.y * cos(d+rot);
    gl_Position = newPosition;
    color = vec3((newPosition.x+1.0)/2.0, (newPosition.y+1.0)/2.0, 0);
}
