attribute vec4 vPosition;
attribute vec3 vColor;

varying lowp vec3 color;

void main(){
    gl_Position = vPosition;
    color = vColor;
}
