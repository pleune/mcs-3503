import Link from 'next/link'
import React, {Component} from 'react';

import initShaderProgram from 'glUtil/initShaderProgram'
import tessTriangle from 'glUtil/tessTriangle'
import {vec2,flatten} from 'glUtil/MV'

import frag from './twist.frag'
import vert from './twist.vert'

class Twist extends Component {
  render() {
    return (
      <div className='row'>
        <h2 className='col'>Twist</h2>
        <div className="w-100" />
        <canvas className='col' id="glCanvasTwist" width="512" height="512" />
      </div>
    )
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const canvas = document.querySelector("#glCanvasTwist")
    const gl = canvas.getContext('webgl')

    if (gl === null) {
      alert('Unable to initialize WebGL. Your browser or machine may not support it.')
      return
    }

    gl.viewport(0, 0, 512, 512);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    var program = initShaderProgram(gl, vert, frag)
    gl.useProgram(program)

    const num_triangle = 5;

    let len = 0.9;
    let points = [
      vec2(len * Math.cos(0),           len * Math.sin(0)),
      vec2(len * Math.cos(2*Math.PI/3), len * Math.sin(2*Math.PI/3)),
      vec2(len * Math.cos(4*Math.PI/3), len * Math.cos(4*Math.PI/3)),
    ]

    let vert_tess = tessTriangle(points[0], points[1], points[2], 30);
    for (let i = 3; i<points.length; i+=3)
    {
      vert_tess.concat(tessTriangle(points[i], points[i+1], points[i+2], 30));
    }

    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vert_tess), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, 'vPosition');
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    gl.clear(gl.COLOR_BUFFER_BIT)
    gl.drawArrays(gl.TRIANGLES, 0, vert_tess.length)
  }

  componentWillUnmount() {}
}

export default Twist
