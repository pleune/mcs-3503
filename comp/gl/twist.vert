attribute vec4 vPosition;
varying lowp vec3 color;

void main(){
    float d = sqrt(vPosition.x*vPosition.x + vPosition.y*vPosition.y);
    vec4 newPosition = vPosition;
    newPosition.x = vPosition.x * cos(d) - vPosition.y * sin(d);
    newPosition.y = vPosition.x * sin(d) + vPosition.y * cos(d);
    gl_Position = newPosition;
    color = vec3((vPosition.x+1.0)/2.0, (vPosition.y+1.0)/2.0, 0);
}
