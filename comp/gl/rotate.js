import Link from 'next/link'
import React, {Component} from 'react';

import initShaderProgram from 'glUtil/initShaderProgram'
import tessTriangle from 'glUtil/tessTriangle'
import {vec2,flatten} from 'glUtil/MV'

import frag from './rotate.frag'
import vert from './rotate.vert'

class Rotate extends Component {
  render() {
    return (
      <div className='row'>
        <h2 className='col'>Rotate</h2>
        <div className="w-100" />
        <canvas className='col' id="glCanvasRotate" width="512" height="512" />
      </div>
    )
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const canvas = document.querySelector("#glCanvasRotate")
    this.gl = canvas.getContext('webgl')

    if (this.gl === null) {
      alert('Unable to initialize WebGL. Your browser or machine may not support it.')
      return
    }

    this.gl.viewport(0, 0, 512, 512)
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0)

    var program = initShaderProgram(this.gl, vert, frag)
    this.gl.useProgram(program)

    const num_triangle = 5

    let len = 0.9;
    let points = [
      vec2(len * Math.cos(0),           len * Math.sin(0)),
      vec2(len * Math.cos(2*Math.PI/3), len * Math.sin(2*Math.PI/3)),
      vec2(len * Math.cos(4*Math.PI/3), len * Math.cos(4*Math.PI/3)),
    ]

    let vert_tess = tessTriangle(points[0], points[1], points[2], 30)
    for (let i = 3; i<points.length; i+=3)
    {
      vert_tess.concat(tessTrianthis.gle(points[i], points[i+1], points[i+2], 30))
    }

    var bufferId = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, bufferId)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, flatten(vert_tess), this.gl.STATIC_DRAW)

    var vPosition = this.gl.getAttribLocation(program, 'vPosition')
    this.gl.vertexAttribPointer(vPosition, 2, this.gl.FLOAT, false, 0, 0)
    this.gl.enableVertexAttribArray(vPosition)

    this.uniform_rot = this.gl.getUniformLocation(program, 'rot')

    this.render_length = vert_tess.length
    this.render_rot = 0.0

    this.draw = this.draw.bind(this)
    this.draw()
  }

  componentWillUnmount() {}

  draw() {
    this.render_rot = this.render_rot + 0.01
    this.gl.uniform1f(this.uniform_rot, this.render_rot)
    this.gl.clear(this.gl.COLOR_BUFFER_BIT)
    this.gl.drawArrays(this.gl.TRIANGLES, 0, this.render_length)
    window.requestAnimationFrame(this.draw)
  }

}

export default Rotate
