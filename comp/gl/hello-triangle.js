import Link from 'next/link'
import React, {Component} from 'react';

import initShaderProgram from 'glUtil/initShaderProgram'

import frag from './hello-triangle.frag'
import vert from './hello-triangle.vert'

class HelloTriangle extends Component {
  render() {
    return (
      <div className='row'>
        <h2 className='col'>Hello Triangle</h2>
        <div className="w-100" />
        <canvas className='col' id="glCanvasHelloTriangle" width="512" height="228" />
      </div>
    )
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const canvas = document.querySelector("#glCanvasHelloTriangle")
    const gl = canvas.getContext('webgl')

    if (gl === null) {
      alert('Unable to initialize WebGL. Your browser or machine may not support it.')
      return
    }

    gl.viewport(0, 0, 512, 228);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    var program = initShaderProgram(gl, vert, frag)
    gl.useProgram(program)

    var vertices = [
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
      Math.random()*2-1, Math.random()*2-1, Math.random()*2-1, Math.random(), Math.random(),
    ]

    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program, 'vPosition');
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 20, 0);
    gl.enableVertexAttribArray(vPosition);

    var vPosition = gl.getAttribLocation(program, 'vColor');
    gl.vertexAttribPointer(vPosition, 3, gl.FLOAT, false, 20, 8);
    gl.enableVertexAttribArray(vPosition);

    gl.clear(gl.COLOR_BUFFER_BIT)
    gl.drawArrays(gl.TRIANGLES, 0, 9)
  }

  componentWillUnmount() {}
}

export default HelloTriangle
