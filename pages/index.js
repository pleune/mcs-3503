import Link from 'next/link'
import HelloTriangle from 'comp/gl/hello-triangle'
import Twist from 'comp/gl/twist'
import Rotate from 'comp/gl/rotate'

export default () => (
  <div className="container">
    <div className="row">
      <div className="col">
        <h1>Mitchell Pleune Graphics Programming</h1>
      </div>
      <div className="w-100" />
    </div>

    <HelloTriangle />
    <Twist />
    <Rotate />
  </div>
)
