import {mix} from './MV'

//
//   tessTriangle.js
//

function tessTriangle( a, b, c, numDivs)
{
    var vertices = [];

    if (numDivs == 1) {
            // Return triangle strip of one triangle
        vertices = [ a, b, c];
    }
    else {
            var percent = 1.0 / numDivs;

            // Calculate very last row of vertices between a and c
            var mixVal = percent;  // starting mix for second point
        var botVerts = [a];  // Add first point
        // Add rest but last point
            for (var i = 0; i<(numDivs-1); i++) {
                botVerts.push(mix(c, a, mixVal));
                mixVal += percent;
            }
        botVerts.push(c);  // Add last point

        // Calculate start values for each row
        mixVal = percent;  // Starting mix for second row
        var startVerts = [];
        // Add start points
        for (i = 0; i<(numDivs-1); i++) {
            startVerts.push(mix(b, a, mixVal));
            mixVal += percent;
        }

        // Calculate end values for each row
        mixVal = percent;  // Starting mix for second row
        var endVerts = [];
        // Add end points
        for (i = 0; i<(numDivs-1); i++) {
            endVerts.push(mix(b, c, mixVal));
            mixVal += percent;
        }

        // Create triangles for each row (starting at bottom)
            // except top triangle
        var botDivs = numDivs;
            for (i = 0; i<(numDivs-1); i++) {
            var topDivs = botDivs-1;
                var topStart = startVerts[i];
                var topEnd = endVerts[i];
                var topVerts = [topStart];
            mixVal = percent = 1.0 / topDivs;

            // Add rest of top except end vertex
                for (var j = 0; j<(topDivs-1); j++) {
                topVerts.push(mix(topEnd, topStart, mixVal));
                mixVal += percent;
            }
            topVerts.push(topEnd);  // Add end vertex

            // Create triangles from botVerts and topVerts
            for (j=0; j<botDivs; j++) {
                vertices.push(botVerts[j]);
                vertices.push(topVerts[j]);
                vertices.push(botVerts[j+1]);
                if (j<topDivs) {
                    vertices.push(botVerts[j+1]);
                    vertices.push(topVerts[j]);
                    vertices.push(topVerts[j+1]);
                }
            }

            // Make top vertices the bottom vertices for next time through loop
            botDivs = topDivs;
            botVerts = topVerts;
            }  // End calculating triangles for each row but the top triangle

        // Add last triangle
        vertices.push(startVerts[numDivs-2]);
        vertices.push(b);
        vertices.push(endVerts[numDivs-2]);

    }  // End calculating triangles

    return vertices;
}

export default tessTriangle
